import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom';
import React, { Component } from 'react'; 
import request from'./components/request.js';
import login from './components/login';
import register from './components/register';
import profile from './components/profile';
import dashboard from './components/dashboard';
import navbar from './components/navbar';
class App extends Component {

  
  render(){
  return (
    <Router>
     <div className="container">
        <div className='row'>
             {/* <div className='col'>
                  <img src="./public/chill.png"></img>
             </div> */}
             <div className='col'>
                <div className="auth-wrapper">
                    <div className="auth-inner">
                      <Switch>
                      <Route path="/request" component={request}></Route>
                      <Route path="/navbar" component={navbar}></Route>
                      <Route path="/login" component={login}></Route>
                      <Route path="/register" component={register}></Route>
                      <Route path="/profile" component={profile}></Route>
                      <Route path="/dashboard" component={dashboard}></Route>
                      <Redirect from="/" to="/login" /> 
                      </Switch>
                    </div>
                </div>  
              </div>
          </div>
        </div>

       </Router>
  );
}
}


export default App;
