import React, { Component } from "react";
import { Button, Col, Row, Container, Form } from 'react-bootstrap';
import {FormGroup,Input,Card,CardBody} from 'reactstrap';
const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class register extends Component{
    
    constructor(props) {
        super(props);
        this.state = { 
            userPassword: '',
            userEmail: '',
            userPhone: '',
            userName: ''         
    };

    this.handleSubmit = this.handleSubmit.bind(this)
   };
   
    async handleSubmit(e){
        e.preventDefault();
        
        const data = {
            userEmail: this.state.userEmail,
            userPassword : this.state.userPassword,
            userName : this.state.userName,
            userPhone : this.state.userPhone,
            userStatus:" "
        }
        //console.log(this.state);
        
        if(this.state.userEmail === "" || this.state.userPassword === "" || 
            this.state.userName === "" || this.state.userPhone === ""){
            alert("All Fields are Mandatory");
        }
        if(re.test(this.state.userEmail) === false ){
            alert("Email is not valid");
        }
        if(this.state.userPassword.length < 8)
        {
            alert("Password must atleast 8 characters");   
        }
        if(this.state.userPhone.length < 10){
            alert("Phone Number must be atleast 10 character");
        }
        else {
            await fetch('http://localhost:8000/user/register', { 
                    method : 'POST' , 
                    headers: {
                        'content-type':'application/json'
                    },        
                    body : JSON.stringify(data)
                    })
                    .then(res => res.json())
                    .then((data) => {
                        console.log(register);
                        console.log(data);
                        if(data.auth === true){
                                    localStorage.setItem("token", data.token);
                                 }
                        // this.props.history.push('/loginForm');
                        window.location.href = '/login'
                       })
                      .then(() => this.setState({ redirect: true }))
                      .then(data => {
                        alert("Registration Success");
                    })
        }                          
        
    };  
               
    render(){
        // const { redirect } = this.state;
        //const {errors} = this.state;
        // if (redirect) {
        //     return <Redirect to='/loginForm'/>;
        // }
        return(
            <div style={{marginTop:'30px'}}>
                <Container>
             
             <Row>
                 <Col xs={12}><h3 className="text-center" style={{marginTop:'4%'}} >Registration Form </h3>
                 </Col>
             </Row>
      
             <Row style={{marginTop:'4%'}}>
                <Col lg="3" md="3" sm="12" xs="12">
                </Col>
              <Col lg="6" md="6" sm="12" xs="12">
            <Card>
                <CardBody style={{backgroundColor:'#fff'}}>
                    <FormGroup>
                        <p style={{textAlign:'left'}} >User Name</p>
                        <Input
                            type="text"
                            name="userName"
                            placeholder="User Name"
                            onChange={(event) => this.setState({userName: event.target.value})}                          
                            ></Input>
                    </FormGroup>
                    
                    <FormGroup>
                        <p style={{textAlign:'left'}} >User Email</p>
                        <Input
                            type="userEmail"
                            name="userEmail"
                            placeholder="User Email"
                            onChange={(event) => this.setState({userEmail: event.target.value})}
                            ></Input>
                    </FormGroup>
                    
                    <FormGroup>
                        <p style={{textAlign:'left'}} > Password</p>
                        <Input
                            type="password"
                            name="userPassword"
                            placeholder="Password" 
                            onChange={(event) => this.setState({userPassword: event.target.value})}
                            ></Input>
                        <Form.Text className="text-muted" style={{textAlign:'left'}}>
                                Password should contain atleast 8 characters
                        </Form.Text>
                    </FormGroup>
                 
                     
                    <FormGroup>
                        <p style={{textAlign:'left'}} >Phone Number</p>
                        <Input
                            type="text"
                            name="userPhone"
                            placeholder="Phone Number"
                            onChange={(event) => this.setState({userPhone: event.target.value})}
                            ></Input>
                    </FormGroup>
                 
                    <Button variant="primary" type="submit" onClick = { this.handleSubmit } active>
                         Submit
                    </Button>
                </CardBody>
            </Card>
            </Col>
              
             </Row>
     
             </Container>
            </div>    

        );
    }
}