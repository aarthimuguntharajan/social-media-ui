import React, { Component } from 'react'; 
import Cookies from 'js-cookie';

class request extends Component{

    constructor(props){
        super(props);
        this.state = {
            requestData : [ ],
            senderName : " ",
            receiverName: " "
            
        };
    this.handleAccept = this.handleAccept.bind(this);
    this.handleReject = this.handleReject.bind(this)  
    };

      handleAccept (name) {
        return (event) => {
            event.preventDefault();
            console.log(name);
            const data = {
                receiverName : Cookies.get('userName'),
                senderName : name
            };
             fetch('http://localhost:8000/user/accept',{
                method: 'POST',
                headers:{
                    'content-type': "application/json",
                    // "x-access-token": Cookies.get('token') 
                },
                body : JSON.stringify(data)
            })
            .then(response=> response.json())
            .then((data) => {
                window.location.href = '/profile';
            })
        }
        

    }

    handleReject(e){
        e.preventDefault();
        //console.log("reject");
    }
    async componentDidMount(){
        await fetch('http://localhost:8000/user/request',{
            method: 'GET',
            headers:{
                'content-type': "application/json",
                // "x-access-token": Cookies.get('token') 
            }
        })
        .then(response=> response.json())
        .then(dataObj => {
            //console.log(dataObj);
            if (dataObj.auth === true){
                //const users[] = dataObj.data.Users
                this.setState({ 
                    requestData : dataObj.data.Requests
                })
            }
            //console.log(this.state.requestData);
        })
        
    }
    render(){
        return(
        <div class=" container justify-content-center">
            <nav aria-label="breadcrumb" class="main-breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Request</li>
                </ol>
            </nav>
           {this.state.requestData.map((data,key) => {
               if(data.receiverName == Cookies.get('userName')){
                    return(
                        <div class="card p-3  mt-3 justify-content-center" key={key} style = {{ width : "800px"}}>
                            <div class="d-flex align-items-center">
                                <div class="image"> <img src="https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80" class="rounded" width="155">
                                    </img> 
                                </div>
                                <div class="ml-3 w-100">
                                    <h4 class="mb-0 mt-0"></h4> <span>{data.senderName} has sent request to you</span>
                                    {/* <div class="p-2 mt-2 bg-primary d-flex justify-content-between rounded text-white stats">
                                        <div class="d-flex flex-column"> 
                                        <span class="articles">Status</span> 
                                    
                                        <span class="number1"></span>  
                                        </div>
                                        {/* <div class="d-flex flex-column"> <span class="followers">Followers</span> <span class="number2">980</span> </div>
                                        <div class="d-flex flex-column"> <span class="rating">Rating</span> <span class="number3">8.9</span> </div> */}
                                    {/* </div> */} 
                                    <div class="button mt-2 pt-3 d-flex flex-row align-items-center">
                                    
                                        <input type="hidden" name="receiverName" class="receiverName" value="{{data.userName}}"></input>
                                        <input type="hidden" name="sender-name" class="sender-name" value="{{Cookies.username}}"></input>
                                        <button class="btn btn-sm btn-outline-primary" onClick={this.handleAccept(data.senderName) }>Accept</button> 
                                        <button class=" btn btn-sm btn-primary ml-2" type="submit" onClick={this.handleReject }> Reject </button> 
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
               }
               if( data.senderName == Cookies.get('userName')){
                return(
                    <div class="card p-3  mt-3 justify-content-center" key={key} style = {{ width : "800px"}}>
                        <div class="d-flex align-items-center">
                            <div class="image"> <img src="https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80" class="rounded" width="155">
                                </img> 
                            </div>
                            <div class="ml-3 w-100">
                                <h4 class="mb-0 mt-0"></h4> <span> You had sent request to {data.receiverName}</span>
                                {/* <div class="p-2 mt-2 bg-primary d-flex justify-content-between rounded text-white stats">
                                    <div class="d-flex flex-column"> 
                                    <span class="articles">Status</span> 
                                
                                    <span class="number1"></span>  
                                    </div>
                                    {/* <div class="d-flex flex-column"> <span class="followers">Followers</span> <span class="number2">980</span> </div>
                                    <div class="d-flex flex-column"> <span class="rating">Rating</span> <span class="number3">8.9</span> </div> */}
                                {/* </div> */} 
                                {/* <div class="button mt-2 pt-3 d-flex flex-row align-items-center">
                                
                                    <input type="hidden" name="receiverName" class="receiverName" value="{{data.userName}}"></input>
                                    <input type="hidden" name="sender-name" class="sender-name" value="{{Cookies.username}}"></input>
                                    <button class="btn btn-sm btn-outline-primary">Accept</button> 
                                    <button class=" btn btn-sm btn-primary ml-2" type="submit"> Reject </button> 
                                
                                </div> */}
                            </div>
                        </div>
                    </div>
                )
               }
           })}
            
        </div>
        
            )       

    }
}
export default request;