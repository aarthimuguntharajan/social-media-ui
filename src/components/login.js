import React, { Component } from "react";
import {  Link } from 'react-router-dom';
import { Button, Col, Row, Container } from 'react-bootstrap';
import {FormGroup,Input,Card,CardBody} from 'reactstrap';
import profile from './profile';
import Cookies from 'js-cookie';
import request from './request.js';

const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


export default class login extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            userPassword:'',
            userEmail:''             
    };
    // if (Cookies.get('token')){
    //     window.location.href= '/request';
    // }
    this.handleSubmit = this.handleSubmit.bind(this)
   };

   
     handleSubmit(e){
        e.preventDefault();
        console.log(e.target);
        const data = {
            userEmail: this.state.userEmail,
            userPassword : this.state.userPassword
        };

        if(this.state.userEmail === "" || this.state.userPassword === "" )

        {
            alert("All Fields are mandatory");

        }
        if(re.test(this.state.userEmail) === false){
            alert("Email is Invalid");
        }
        if(this.state.userPassword.length < 8)
        {
            alert("userPassword must atleast 8 characters");   
        }
        else{
             fetch('http://localhost:8000/user/login', { 
                        method : 'POST' , 
                        headers: {
                            'content-type':'application/json'
                        },        
                        body : JSON.stringify(data)
                        })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);
                            if(data.msg === "Bad payload"){
                                alert("shouldnt be empty");
                            }
                            else if(data.msg !== "Bad payload" ){
                                //const pwd =  bcrypt.hashSync(this.state.userPassword, 8)
                                Cookies.set("token", data.token);
                                Cookies.set("userEmail", data.data.userEmail);
                                Cookies.set("userId", data.data.id);
                                Cookies.set("userPhone", data.data.userPhone);
                                Cookies.set("userName", data.data.userName);
                                Cookies.set("userPassword",data.data.userPassword);
                                alert("Login success");
                                //this.props.history.push('/profile');
                               window.location.href = '/dashboard'
                            }
                        })                       
            }               
        
    };                                              


    render() {

        return (
            <div style={{marginTop:'30px'}}>

      
            <Container>
            
            <Row>
                <Col xs={12}><h3 className="text-center" style={{marginTop:'4%'}} >User Login </h3>
                </Col>
            </Row>

            <Row style={{marginTop:'4%'}}>
            <Col lg="3" md="3" sm="12" xs="12">
                </Col>
            <Col lg="6" md="6" sm="12" xs="12">
            
            <Card>
            <CardBody style={{backgroundColor:'#fff'}}>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <p style={{textAlign:'left'}} >User Email</p>
                        <Input
                            type="userEmail"
                            name="userEmail"
                            placeholder="User Email"
                            value = {this.state.userEmail}
                            onChange={(event) => this.setState({userEmail: event.target.value})}                            
                        />
                    </FormGroup>
                    <FormGroup>
                        <p style={{textAlign:'left'}} >Password</p>
                        <Input
                            type="password"
                            name="userPassword"
                            placeholder=" Enter Password"
                           value = {this.state.userPassword}
                            onChange={(event) => this.setState({userPassword: event.target.value})}
                            />
                    </FormGroup>
                    <Link className="nav-link" to={"/register"}>New User? Register here</Link>
                    <Button  variant="primary" type="submit" onClick = { this.handleSubmit } active>  
                            Submit
                    </Button>
                </form>    
                </CardBody>
                </Card>
                </Col>
                </Row>
                </Container>
            
         </div>
        );
    }
}