import React from 'react'; 

class navbar extends React.Component{
render(){
    return(
        <nav className="navbar  navbar-light" style={{ backgroundColor: '#e3f2fd' ,textAlign : 'left', fontWeight:'bold',fontSize:'20px'}}>
            <div class="col-3">
                <a class="navbar-brand" href="#">
                    <img src="/docs/4.0/assets/brand/bootstrap-solid.svg" width="30" height="30" alt="">
                    </img> Quarantine And Chill
                </a>
            </div>
            <div class="col-6">
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
            <div className='col-1'>
                    Profile
                
            </div>
            <div className ='col-1'>
                    Request
            </div>
            <div class="col-1" style={{ textAlign : 'right'}}>
                    LOGOUT
            </div>
        </nav>
    );
}
}
export default navbar